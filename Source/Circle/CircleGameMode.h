// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CircleGameMode.generated.h"

UCLASS(minimalapi)
class ACircleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACircleGameMode();
};



