// Copyright Epic Games, Inc. All Rights Reserved.

#include "CircleGameMode.h"
#include "CircleHUD.h"
#include "CircleCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACircleGameMode::ACircleGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ACircleHUD::StaticClass();
}
